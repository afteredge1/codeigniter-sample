# CodeIgniter 4 Sample

## Configuration steps
- clone project
- install packages with composer
- create database with name tribehire_backend
- configure .env file with database details
- php spark migrate (command to migrate database tables)


## API end point to import data
http://localhost/api-sample/public/import

## API end points 
http://localhost/api-sample/public/postlist

http://localhost/api-sample/public/search/laborum

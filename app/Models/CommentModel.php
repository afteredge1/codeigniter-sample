<?php

namespace App\Models;

use CodeIgniter\Model;

class CommentModel extends Model
{
    protected $table = 'comments';
    protected $allowedFields = ['postId', 'name', 'email', 'body'];

    public function add($data) {
        return $this->insertBatch($data);
    }
    
    public function search($slug)
    {
        return $this->like('name', $slug)
                    ->orLike('email', $slug)
                    ->orLike('body', $slug)
                    ->get()->getResultArray();
    }
}
<?php

namespace App\Models;

use CodeIgniter\Model;

class PostModel extends Model
{
    protected $table = 'posts';
    protected $allowedFields = ['userId', 'title', 'body'];

    public function add($data) {
        return $this->insertBatch($data);
    }

    public function get()
    {
        return $this->query("SELECT p.post_id, p.title, p.body,
                                (SELECT COUNT(*) FROM comments c WHERE c.postId=p.post_id) comment_count
                                FROM posts p ORDER BY comment_count DESC")->getResultArray();
    }
    
}
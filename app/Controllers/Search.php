<?php

namespace App\Controllers;

use App\Models\CommentModel;

class Search extends BaseController
{
    public function __construct()
    {
        $this->model_comment = new CommentModel(); 
    }

    public function index($slug = '')
    {
        $uri = service('uri');
        $slug =  ($uri->getSegment(2)) ? $uri->getSegment(2) : '';
        $comments = $this->model_comment->search($slug);
        
        echo json_encode($comments);
    }
}

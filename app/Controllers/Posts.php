<?php

namespace App\Controllers;

use App\Models\PostModel;

class Posts extends BaseController
{
    public function __construct()
    {
        $this->model_post = new PostModel();
    }

    public function index()
    {
        $posts = $this->model_post->get();
        echo json_encode($posts);
    }
}

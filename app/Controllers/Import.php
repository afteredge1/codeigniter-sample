<?php

namespace App\Controllers;

use App\Models\CommentModel;
use App\Models\PostModel;

class Import extends BaseController
{
    public function __construct()
    {
        $this->model_post = new PostModel();
        $this->model_comment = new CommentModel(); 
    }

    public function index()
    {
        // import posts
        $posts = json_decode(file_get_contents('https://jsonplaceholder.typicode.com/posts'));
        $this->model_post->add($posts);

        // import comments
        $comments = json_decode(file_get_contents('https://jsonplaceholder.typicode.com/comments'));                
        $this->model_comment->add($comments);

        return 'successfull';
    }
}

<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddComments extends Migration
{
        public function up()
        {
            // postId
            // id
            // name
            // email
            // body                
            $this->forge->addField([
                    'comment_id'          => [
                            'type'           => 'INT',
                            'constraint'     => 11,
                            'unsigned'       => true,
                            'auto_increment' => true,
                    ],
                    'postId' => [
                            'type'       => 'INT',
                            'constraint' => 11,
                    ],
                    'name'       => [
                            'type'       => 'VARCHAR',
                            'constraint' => '255',
                    ],
                    'email'       => [
                            'type'       => 'VARCHAR',
                            'constraint' => '100',
                    ],
                    'body' => [
                            'type' => 'TEXT',
                            'null' => true,
                    ],
            ]);
            $this->forge->addKey('comment_id', true);
            $this->forge->createTable('comments');
        }

        public function down()
        {
                $this->forge->dropTable('comments');
        }
}
<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddPosts extends Migration
{
        public function up()
        {
                // userId
                // id
                // title
                // body
                $this->forge->addField([
                        'post_id'          => [
                                'type'           => 'INT',
                                'constraint'     => 11,
                                'unsigned'       => true,
                                'auto_increment' => true,
                        ],
                        'userId'       => [
                                'type'       => 'INT',
                                'constraint' => 11,
                        ],
                        'title' => [
                                'type' => 'VARCHAR',
                                'constraint' => 255
                        ],
                        'body' => [
                                'type' => 'TEXT',
                                'null' => true,
                        ],
                ]);
                $this->forge->addKey('post_id', true);
                $this->forge->createTable('posts');
        }

        public function down()
        {
                $this->forge->dropTable('posts');
        }
}